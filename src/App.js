// import './App.css';
import React, { useState, useEffect } from 'react';
import AddTask from './components/AddTask';
import Footer from './components/Footer';
import Header from './components/Header';
import Tasks from './components/Tasks';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import About from './components/About';

function App() {
  const [showForm, setShowForm] = useState(false);
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    const getTasks = async () => {
      const tasks_data = await fetchTasks();
      setTasks(tasks_data);
    };
    getTasks();
  }, []);

  // Get Task from Server
  const fetchTasks = async () => {
    const result = await fetch('http://localhost:5000/tasks').catch(e => console.log(e));
    const data = await result.json();
    return data;
  };

  const fetchTask = async (id) => {
    const result = await fetch(`http://localhost:5000/tasks/${id}`).catch(e => console.log(e));
    const data = await result.json();
    return data;
  };

  const handleDelete = async (id) => {
    await fetch(`http://localhost:5000/tasks/${id}`, { method: 'DELETE' }).catch(e => console.log(e));
    setTasks(tasks.filter((task) => task.id !== id));
  };

  const toggleReminder = async (id) => {
    const old_task = await fetchTask(id);
    const updated_task = { ...old_task, reminder: !old_task.reminder };
    const res = await fetch(`http://localhost:5000/tasks/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updated_task),
    }).catch(e => console.log(e));
    const data = await res.json();
    setTasks(
      tasks.map((task) =>
        task.id === id ? { ...task, reminder: data.reminder } : task
      )
    );
    // console.log(tasks.find((task) => task.id === id));
    // console.log(`Reminder: ${id}`);
  };

  const addTask = async (task) => {
    // console.log(task);
    // const id = Math.floor(Math.random() * 10000 + 1);
    // const newTask = { id, ...task };
    const res = await fetch('http://localhost:5000/tasks', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(task),
    });
    const data = await res.json();
    setTasks([...tasks, data]);
  };
  return (
    <Router>
      <div className='container'>
        <Header
          title='Tasks Tracker'
          onAdd={() => setShowForm(!showForm)}
          setButtonTitle={showForm}
        />
        {showForm && <AddTask addTask={addTask} />}
                {tasks.length > 0 ? (
                  <Tasks
                    tasks={tasks}
                    onDelete={handleDelete}
                    onToggleReminder={toggleReminder}
                  />
                ) : (
                  'Empty Tasks List'
                )}
        <Routes>
          {/* <Route
            path='/add-task'
            exact
            component={(props) => (
              <>
                
              </>
            )}
          /> */}
          <Route path='/about' element={<About />} />
        </Routes>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
