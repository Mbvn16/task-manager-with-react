import Task from './Task';

const Tasks = ({ tasks, onDelete, onToggleReminder }) => {
  return (
    <div>
      <ul>
        {' '}
        {tasks.map((task) => (
          <Task
            key={task.id}
            task={task}
            onDelete={onDelete}
            onToggle={onToggleReminder}
          />
        ))}{' '}
      </ul>{' '}
    </div>
  );
};

export default Tasks;
