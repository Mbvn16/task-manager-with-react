import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';
import { useLocation } from 'react-router';
const Header = ({ title, onAdd, setButtonTitle }) => {
  const location = useLocation();
  return (
    <header className='header'>
      <h1> {title} </h1>
      {location.pathname === '/' && <Button
        title={setButtonTitle ? 'Close' : 'Add'}
        color={setButtonTitle ? 'red' : 'green'}
        onClick={onAdd}
      />}{' '}
    </header>
  );
};
Header.defaultProps = {
  title: 'Hello Task users',
};
Header.propTypes = {
  title: PropTypes.string,
};
export default Header;
