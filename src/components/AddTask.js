import { useState } from 'react';

const AddTask = ({ addTask }) => {
  const [text, setText] = useState('');
  const [day, setDay] = useState('');
  const [reminder, setReminder] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();
    if (text === '') {
      alert('Please enter task text');
    }

    addTask({ text, day, reminder });
    setText('');
    setDay('');
    setReminder(false);
  };

  return (
    <div>
      <form className='add-form' onSubmit={onSubmit}>
        <div className='form-control'>
          <label>Text</label>
          <input
            type='text'
            value={text}
            onChange={(e) => setText(e.target.value)}
            autoFocus
          />
        </div>
        <div className='form-control'>
          <label>Day</label>
          <input
            type='text'
            value={day}
            onChange={(e) => setDay(e.target.value)}
          />
        </div>
        <div className='form-control form-control-check'>
          <label>Reminder</label>
          <input
            type='checkbox' checked={reminder}
            value={reminder}
            onChange={(e) => setReminder(e.currentTarget.checked)}
          />
        </div>
        <input type='submit' className='btn btn-block' value='Save Task' />
      </form>
    </div>
  );
};

export default AddTask;
