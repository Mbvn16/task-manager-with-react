import PropTypes from 'prop-types';

const Button = ({ title, color, onClick }) => {
    // const onClick = (e) => {
    //     console.log('Clicked', e);
    // };
    return ( 
        <button style = {
            { backgroundColor: color } }
        onClick = { onClick }
        className = 'btn' >
        { ' ' } { title } { ' ' } 
        </button>
    );
};

Button.propTypes = {
    color: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    title: 'Add',
    color: 'black',
};

export default Button;